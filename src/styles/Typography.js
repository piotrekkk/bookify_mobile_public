export const FontSizes = {
  small: 14,
  base: 16,
  medium: 18,
  large: 20,
  xLarge: 22,
  xxLarge: 26,
};

export const FontWeights = {
  medium: '500',
  semiBold: '600',
  bold: '700',
};
