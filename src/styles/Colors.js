export default {
  greyLight: '#282d32',
  greyDark: '#181b1e',
  purple: '#698bfc',
  purpleDark: '#2d335f',
  white: '#ffffff',
  green: '#11b719',
  red: '#cc0000',
};
