import React, { useEffect, useState } from 'react';
import { Image, Linking, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';
import Colors from '../styles/Colors';
import { Dimensions } from 'react-native';
import { FontSizes, FontWeights } from '../styles/Typography';
import Variables from '../styles/Variables';
import { indexServicesDataAction } from '../actions/Services';
import ListItem from '../components/ListItem';

const CompanyScreen = props => {
  const { navigation, services } = props;
  const [companyId, setCompanyId] = useState(null);
  const [company, setCompany] = useState({});

  useEffect(() => {
    const { companyId } = props.route.params;
    const company = props.companies['all'].collection.find(companyItem => companyItem.id === companyId);

    setCompanyId(companyId);
    setCompany(company)
    props.indexServicesDataAction({ companyId });

    navigation.setOptions({ title: company.name });
  }, []);

  const openAddress = address => {
    const param = encodeURIComponent(address);
    Linking.openURL(`https://www.google.com/maps/search/?api=1&query=${param}`);
  }

  return (
    <SafeAreaView style={style.safeAreaWrapper}>
      <ScrollView style={style.wrapper}>
        {company.background_image_url && (
          <Image
            style={style.companyImage}
            resizeMode={'cover'}
            source={{url: company.background_image_url}}
          />
        )}
        <Text style={style.companyName}>{company.name}</Text>
        <Text
          style={style.companyAddress}
          onPress={() => openAddress(company.address)}
        >
          {company.address}, Kraków
        </Text>
        {company.description && (
          <Text style={style.companyDescription}>{company.description}</Text>
        )}

        <Text style={style.header}>Usługi</Text>
        {services[companyId] && services[companyId].collection.map((service, index) =>
            <ListItem
              key={index}
              name={service.name}
              rightText={service.price}
              onPress={() =>
                navigation.navigate('Service', {
                  companyId,
                  serviceId: service.id,
                })
              }
            />
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  safeAreaWrapper: {
    flex: 1,
    backgroundColor: Colors.greyLight,
  },
  wrapper: {
    padding: 10,
  },
  header: {
    marginBottom: 10,
    fontSize: FontSizes.medium,
    fontWeight: FontWeights.medium,
    color: Colors.purple,
  },
  companyImage: {
    width: Dimensions.get('window').width - 20,
    height: ((Dimensions.get('window').width - 20) * 2) / 3,
    borderRadius: Variables.borderRadius,
  },
  companyName: {
    marginTop: 20,
    marginBottom: 5,
    fontSize: FontSizes.xxLarge,
    fontWeight: FontWeights.semiBold,
    color: Colors.purple,
  },
  companyAddress: {
    marginBottom: 20,
    fontSize: FontSizes.small,
    color: Colors.purple,
  },
  companyDescription: {
    marginBottom: 20,
    fontSize: FontSizes.base,
    color: Colors.white,
  },
});

const mapStateToProps = state => ({
  companies: state.companies,
  businessTypes: state.businessTypes,
  services: state.services
});

const mapDispatchToProps = dispatch => ({
  indexServicesDataAction: data => dispatch(indexServicesDataAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(CompanyScreen);
