import React, { Component } from 'react';
import { SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, View } from 'react-native';
import { indexCompaniesDataAction } from '../actions/Companies';
import { indexBusinessTypesDataAction } from '../actions/BusinessTypes';
import { connect } from 'react-redux';
import Colors from '../styles/Colors';
import { FontSizes } from '../styles/Typography';
import CompanyItem from '../components/CompanyItem';

class RootScreen extends Component {
  state = {
    businessTypeId: 'all',
  }

  componentDidMount() {
    const { navigation } = this.props;

    this.props.indexBusinessTypesAction();

    this._onFocusListener = navigation.addListener('focus', () => {
      const routeParams = this.props.route.params;

      if (routeParams && routeParams.businessTypeId) {
        this.setState({ businessTypeId: routeParams.businessTypeId }, () => this.props.indexCompaniesAction({ businessTypeId: this.state.businessTypeId }));
      } else {
        this.props.indexCompaniesAction({ businessTypeId: this.state.businessTypeId });
      }
    });
  }

  componentWillUnmount() {
    this._onFocusListener();
  }

  navigateToBusinessTypePicker() {
    this.props.navigation.navigate('BusinessTypePicker');
  }

  navigateToCompany(companyId) {
    this.props.navigation.navigate('Company', { companyId });
  }

  getBusinessTypeNameButton() {
    const { businessTypeId } = this.state;
    const { businessTypes } = this.props;
    let businessTypeName = '';

    if (businessTypeId === 'all') {
      businessTypeName = 'usługi';
    } else {
      businessTypeName = businessTypes.collection.find(type => type.id === businessTypeId).name;
    }

    return (
      <View>
        <Text
          style={style.filtersButton}
          onPress={() => this.navigateToBusinessTypePicker()}
        >
          {businessTypeName}
        </Text>
      </View>
    );
  }

  render() {
    const { businessTypeId } = this.state;
    const { navigation } = this.props;

    return (
      <SafeAreaView style={style.safeAreaWrapper}>
        <View style={style.header}>
          <View style={style.filtersWrapper}>
            <Text style={style.filtersText}>Szukasz</Text>
            {this.getBusinessTypeNameButton()}
            <Text style={style.filtersText}>w</Text>
            <View>
              <Text style={style.filtersButton}>Kraków</Text>
            </View>
          </View>

          <Text
            style={style.historyText}
            onPress={() => navigation.navigate('History')}
          >
            Historia
          </Text>
        </View>

        <ScrollView style={style.wrapper}>
          <StatusBar barStyle="light-content" />

          {this.props.companies[businessTypeId] && this.props.companies[businessTypeId].collection.map((company, index) => (
            <CompanyItem
              itemStyle={{marginTop: index > 0 ? 10 : 0}}
              key={index} company={company}
              onPress={() => this.navigateToCompany(company.id)}
            />
          ))}

          {this.props.companies[businessTypeId] && !this.props.companies[businessTypeId].collection.length && (
            <Text style={style.emptyText}>Brak wyników</Text>
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  safeAreaWrapper: {
    flex: 1,
    backgroundColor: Colors.greyLight,
  },
  wrapper: {
    padding: 10,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
  },
  filtersWrapper: {
    flexDirection: 'row',
  },
  filtersText: {
    color: Colors.white,
  },
  filtersButton: {
    marginLeft: 5,
    marginRight: 5,
    color: Colors.purple,
  },
  historyText: {
    color: Colors.purple,
  },
  emptyText: {
    alignSelf: 'center',
    fontSize: FontSizes.large,
    color: Colors.purple,
  },
});

const mapStateToProps = state => ({
  companies: state.companies,
  businessTypes: state.businessTypes
});

const mapDispatchToProps = dispatch => ({
  indexCompaniesAction: data => dispatch(indexCompaniesDataAction(data)),
  indexBusinessTypesAction: data => dispatch(indexBusinessTypesDataAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(RootScreen);
