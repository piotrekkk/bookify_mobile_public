import React, { Component } from 'react';
import { ActivityIndicator, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Colors from '../styles/Colors';
import { connect } from 'react-redux';
import { FontSizes, FontWeights } from '../styles/Typography';
import FormInput from '../components/FormInput';
import Variables from '../styles/Variables';
import { createVisitAction } from '../actions/Visits';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import { getUniqueId } from 'react-native-device-info';

class BookVisitScreen extends Component {
  state = {
    isLoading: false,
    companyId: '',
    serviceId: '',
    date: '',
    company: {},
    service: {},
    user: {
      first_name: '',
      last_name: '',
      phone: '',
      email: '',
    },
  };

  componentDidMount() {
    const { navigation } = this.props;
    const { companyId, serviceId, date } = this.props.route.params;
    const company = this.props.companies['all'].collection.find(companyItem => companyItem.id === companyId);
    const service = this.props.services[companyId].collection.find(serviceItem => serviceItem.id === serviceId);
    this.setState({ date, companyId, serviceId, company, service });

    navigation.setOptions({ title: 'Zarezerwuj wizytę' });

    this.trySetCurrentUserData();
  }

  async trySetCurrentUserData() {
    const first_name = await AsyncStorage.getItem('@user_first_name');
    const last_name = await AsyncStorage.getItem('@user_last_name');
    const email = await AsyncStorage.getItem('@user_email');
    const phone = await AsyncStorage.getItem('@user_phone');

    if (first_name || last_name || email || phone) {
      this.setState({ user: { first_name, last_name, email, phone }});
    }
  }

  validateForm() {
    const { first_name, last_name, phone, email } = this.state.user;
    return !!first_name && !!last_name && !!phone && !!email;
  }

  handleBookVisit() {
    const { date, companyId, serviceId, user } = this.state;
    const uniqDeviceId = getUniqueId();
    this.setState({ isLoading: true });

    this.props.createVisitAction({ date, companyId, serviceId, user, uniqDeviceId }).then(async () => {
      this.setState({ isLoading: false });

      Snackbar.show({
        text: 'Wizyta zarezerwowana!',
        duration: Snackbar.LENGTH_LONG,
        textColor: Colors.white,
        backgroundColor: Colors.green,
      });

      await AsyncStorage.setItem('@user_first_name', user.first_name);
      await AsyncStorage.setItem('@user_last_name', user.last_name);
      await AsyncStorage.setItem('@user_email', user.email);
      await AsyncStorage.setItem('@user_phone', user.phone);

      this.props.navigation.navigate('Root');
    }).catch(() => {
      this.setState({ isLoading: false });

      Snackbar.show({
        text: 'Wystąpił błąd. Spróbuj ponownie póżniej',
        duration: Snackbar.LENGTH_LONG,
        textColor: Colors.white,
        backgroundColor: Colors.red,
      });
    });
  }

  render() {
    const { isLoading, date, company, service, user } = this.state;
    const isDisabled = !this.validateForm() || isLoading;

    return (
      <SafeAreaView style={style.safeAreaWrapper}>
        <ScrollView style={style.wrapper}>
          <Text style={style.serviceName}>{date && date.day} - {date && date.hour}</Text>
          <Text style={style.serviceDescriptionItem}>Usługa: {service.name}</Text>
          <Text style={style.serviceDescriptionItem}>Adres: {company.address}</Text>
          <Text style={style.serviceDescriptionItem}>Cena: {service.price} zł</Text>

          <Text style={style.header}>Uzupełnij swoje dane</Text>

          <View style={style.formRow}>
            <FormInput
              name="Imię"
              defaultValue={user.first_name}
              onChange={(value) =>
                this.setState({user: {...this.state.user, first_name: value}})
              }
            />
            <FormInput
              name="Nazwisko"
              defaultValue={user.last_name}
              customStyle={{marginLeft: 20}}
              onChange={(value) =>
                this.setState({user: {...this.state.user, last_name: value}})
              }
            />
          </View>
          <View style={style.formRow}>
            <FormInput
              name="Telefon"
              defaultValue={user.phone}
              textContentType={'telephoneNumber'}
              keyboardType={'number-pad'}
              onChange={(value) =>
                this.setState({user: {...this.state.user, phone: value}})
              }
            />
          </View>
          <View style={style.formRow}>
            <FormInput
              name="Email"
              defaultValue={user.email}
              textContentType={'emailAddress'}
              keyboardType={'email-address'}
              autoCapitalize={'none'}
              onChange={(value) =>
                this.setState({user: {...this.state.user, email: value}})
              }
            />
          </View>

          <TouchableOpacity
            style={style.button}
            activeOpacity={0.8}
            disabled={isDisabled}
            onPress={() => this.handleBookVisit()}>
            {isLoading ? (
              <ActivityIndicator size="small" color={Colors.white} />
            ) : (
              <Text style={style.buttonText}>Zarezerwuj</Text>
            )}
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  safeAreaWrapper: {
    flex: 1,
    backgroundColor: Colors.greyLight,
  },
  wrapper: {
    padding: 10,
  },
  header: {
    marginTop: 20,
    fontSize: FontSizes.medium,
    fontWeight: FontWeights.medium,
    color: Colors.purple,
  },
  serviceName: {
    marginBottom: 5,
    fontSize: FontSizes.xxLarge,
    fontWeight: FontWeights.semiBold,
    color: Colors.purple,
  },
  serviceDescriptionItem: {
    marginBottom: 5,
    fontSize: FontSizes.small,
    color: Colors.purple,
  },
  formRow: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 10,
  },
  button: {
    minHeight: 48,
    marginTop: 20,
    padding: 10,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: Colors.purple,
    borderWidth: 2,
    borderRadius: Variables.borderRadius,
    backgroundColor: Colors.purple,
  },
  buttonText: {
    fontSize: FontSizes.large,
    color: Colors.white,
  }
});

const mapStateToProps = state => ({
  companies: state.companies,
  services: state.services
});

const mapDispatchToProps = dispatch => ({
  createVisitAction: data => dispatch(createVisitAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(BookVisitScreen);
