import React, { useEffect } from 'react';
import { SafeAreaView, ScrollView, StyleSheet, Text } from 'react-native';
import Colors from '../styles/Colors';
import { getUniqueId } from 'react-native-device-info';
import { connect } from 'react-redux';
import { indexVisitsDataAction } from '../actions/Visits';
import HistoryItem from '../components/HistoryItem';
import { FontSizes } from '../styles/Typography';

const HistoryScreen = props => {
  const { navigation, visits, indexVisitsAction } = props;
  const uniqDeviceId = getUniqueId();

  useEffect(() => {
    navigation.setOptions({ title: 'Historia' });
    indexVisitsAction({ uniqDeviceId });
  }, []);

  return (
    <SafeAreaView style={style.safeAreaWrapper}>
      <ScrollView style={style.wrapper}>
        {visits && visits.collection && visits.collection.map(visit => (
          <HistoryItem key={visit.id} visit={visit} />
        ))}

        {visits && !visits.collection && (
          <Text style={style.emptyText}>Brak wyników</Text>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  safeAreaWrapper: {
    flex: 1,
    backgroundColor: Colors.greyLight,
  },
  wrapper: {
    padding: 10,
  },
  emptyText: {
    alignSelf: 'center',
    fontSize: FontSizes.large,
    color: Colors.purple,
  },
});

const mapStateToProps = state => ({
  visits: state.visits
});

const mapDispatchToProps = dispatch => ({
  indexVisitsAction: data => dispatch(indexVisitsDataAction(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(HistoryScreen);
