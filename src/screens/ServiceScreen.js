import React, { Component } from 'react';
import { SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import Colors from '../styles/Colors';
import { connect } from 'react-redux';
import { FontSizes, FontWeights } from '../styles/Typography';
import { Dimensions } from 'react-native';
import ServiceScheduler from '../components/ServiceScheduler';

class ServiceScreen extends Component {
  state = {
    companyId: '',
    serviceId: '',
    company: {},
    service: {},
  };

  componentDidMount() {
    const { navigation } = this.props;
    const { companyId, serviceId } = this.props.route.params;
    const company = this.props.companies['all'].collection.find(company => company.id === companyId);
    const service = this.props.services[companyId].collection.find(service => service.id === serviceId);
    this.setState({ companyId, serviceId, company, service });

    navigation.setOptions({ title: service.name });
  }

  handlePickSlot = date => {
    const { companyId, serviceId } = this.state;
    this.props.navigation.navigate('BookVisit', { date, companyId, serviceId });
  }

  render() {
    const { service } = this.state;

    return (
      <SafeAreaView style={style.safeAreaWrapper}>
        <ScrollView>
          <View style={style.headerContentWrapper}>
            <Text style={style.serviceName}>{service.price} zł</Text>
            <Text style={style.serviceDuration}>
              Czas trwania: {service.duration} minut
            </Text>
            {service.description && (
              <Text style={style.serviceDescription}>
                {service.description}
              </Text>
            )}

            <Text style={style.header}>Dostępne terminy</Text>
          </View>

          <ServiceScheduler
            slots={service.slots}
            onPickSlot={this.handlePickSlot}
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const style = StyleSheet.create({
  safeAreaWrapper: {
    flex: 1,
    backgroundColor: Colors.greyLight,
  },
  headerContentWrapper: {
    paddingLeft: 10,
    paddingTop: 10,
    paddingRight: 10,
  },
  header: {
    fontSize: FontSizes.medium,
    fontWeight: FontWeights.medium,
    color: Colors.purple,
  },
  serviceName: {
    marginBottom: 5,
    fontSize: FontSizes.xxLarge,
    fontWeight: FontWeights.semiBold,
    color: Colors.purple,
  },
  serviceDuration: {
    marginBottom: 20,
    fontSize: FontSizes.small,
    color: Colors.purple,
  },
  serviceDescription: {
    marginBottom: 20,
    fontSize: FontSizes.base,
    color: Colors.white,
  }
});

const mapStateToProps = state => ({
  companies: state.companies,
  services: state.services
});

export default connect(mapStateToProps, null)(ServiceScreen);
