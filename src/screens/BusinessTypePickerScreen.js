import React from 'react';
import { SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import Colors from '../styles/Colors';
import { connect } from 'react-redux';
import ListItem from '../components/ListItem';

const BusinessTypePickerScreen = props => {
  const { businessTypes, navigation } = props;

  return (
    <SafeAreaView style={style.safeAreaWrapper}>
      <ScrollView>
        {businessTypes.collection.map((type, index) => (
          <ListItem
            key={index}
            name={type.name}
            onPress={() =>
              navigation.navigate('Root', {businessTypeId: type.id})
            }
          />
        ))}
      </ScrollView>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  safeAreaWrapper: {
    flex: 1,
    backgroundColor: Colors.greyLight,
  },
});

const mapStateToProps = state => ({
  businessTypes: state.businessTypes
});

export default connect(mapStateToProps, null)(BusinessTypePickerScreen);
