import React from 'react';
import { StyleSheet, Text, TextInput, View } from 'react-native';
import Colors from '../styles/Colors';
import { FontSizes } from '../styles/Typography';
import Variables from '../styles/Variables';

const FormInput = props => {
  const { name, defaultValue, customStyle, keyboardType, textContentType, onChange } = props;
  const autoCapitalize = props.autoCapitalize || 'sentences';

  return (
    <View style={[style.wrapper, customStyle]}>
      <Text style={style.label}>{name}</Text>
      <TextInput
        style={style.input}
        defaultValue={defaultValue}
        autoCapitalize={autoCapitalize}
        keyboardType={keyboardType}
        textContentType={textContentType}
        onChangeText={onChange}
      />
    </View>
  );
};

const style = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  label: {
    marginBottom: 5,
    fontSize: FontSizes.small,
    color: Colors.white,
  },
  input: {
    padding: 10,
    fontSize: FontSizes.medium,
    color: Colors.white,
    borderColor: Colors.purple,
    borderWidth: 2,
    borderRadius: Variables.borderRadius,
    backgroundColor: Colors.purpleDark,
  },
});

export default FormInput;
