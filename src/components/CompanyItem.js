import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Colors from '../styles/Colors';
import { FontSizes, FontWeights } from '../styles/Typography';
import Variables from '../styles/Variables';

const CompanyItem = props => {
  const { company, itemStyle, onPress } = props;

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={[style.wrapper, itemStyle]}
      onPress={onPress}
    >
      {company.background_image_url && (
        <Image
          style={style.image}
          resizeMode={'cover'}
          source={{url: company.background_image_url}}
        />
      )}
      <View style={style.textWrapper}>
        <Text style={style.textCompanyName}>{company.name}</Text>
        <Text style={style.textAddress}>{company.address}</Text>
        <Text style={style.textPrice}>od {company.minimum_price} zł</Text>
      </View>
    </TouchableOpacity>
  );
};

const style = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    padding: 10,
    backgroundColor: Colors.purpleDark,
    borderRadius: Variables.borderRadius,
  },
  textWrapper: {
    flex: 1,
    marginLeft: 10,
  },
  textCompanyName: {
    fontSize: FontSizes.medium,
    fontWeight: FontWeights.bold,
    color: Colors.white,
  },
  textAddress: {
    marginTop: 5,
    fontSize: FontSizes.small,
    color: Colors.white,
    opacity: 0.7,
  },
  textPrice: {
    marginTop: 5,
    marginLeft: 'auto',
    fontSize: FontSizes.base,
    fontWeight: FontWeights.bold,
    color: Colors.purple,
  },
  image: {
    width: 93,
    height: 70,
    borderRadius: Variables.borderRadius / 2,
  },
});

export default CompanyItem;
