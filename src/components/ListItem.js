import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Colors from '../styles/Colors';
import { FontSizes } from '../styles/Typography';

const ListItem = props => {
  const { name, rightText, onPress } = props;

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      style={style.wrapper}
      onPress={onPress}
    >
      <Text style={style.name}>{name}</Text>
      {rightText && <Text style={style.rightText}>{rightText} zł</Text>}
    </TouchableOpacity>
  );
};

const style = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    backgroundColor: Colors.purpleDark,
    borderTopWidth: 1,
    borderColor: Colors.greyLight,
  },
  name: {
    flex: 1,
    fontSize: FontSizes.base,
    color: Colors.purple,
  },
  rightText: {
    fontSize: FontSizes.small,
    color: Colors.white,
  },
});

export default ListItem;
