import React from 'react';
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Colors from '../styles/Colors';
import { FontSizes } from '../styles/Typography';
import { Dimensions } from 'react-native';
import Variables from '../styles/Variables';

const ServiceScheduler = props => {
  const { slots, onPickSlot } = props;

  return (
    <ScrollView
      style={style.wrapper}
      horizontal={true}
      pagingEnabled={true}
      alwaysBounceVertical={true}
    >
      {slots && slots.map((daySlot, dayIndex) =>
        <View style={style.column} key={dayIndex}>
          <Text style={style.daySlotHeader}>{daySlot.day}</Text>

          { daySlot.slots.map((slot, slotIndex) =>
            <TouchableOpacity
              style={style.singleSlotWrapper}
              key={slotIndex}
              activeOpacity={0.8}
              onPress={() => onPickSlot({hour: slot, day: daySlot.day})}
            >
              <Text style={style.singleSlot} numberOfLines={1}>{slot}</Text>
            </TouchableOpacity>
          )}
        </View>
      )}
    </ScrollView>
  );
};

const style = StyleSheet.create({
  scheduler: {
    flex: 1,
    overflow: 'hidden',
  },
  column: {
    width: Dimensions.get('window').width / 4,
    padding: 10,
    alignItems: 'center',
  },
  daySlotHeader: {
    fontSize: FontSizes.medium,
    color: Colors.white,
  },
  singleSlotWrapper: {
    width: 75,
    marginTop: 10,
    padding: 10,
    alignItems: 'center',
    backgroundColor: Colors.purpleDark,
    borderRadius: Variables.borderRadius,
  },
  singleSlot: {
    fontSize: FontSizes.medium,
    color: Colors.purple,
  },
});

export default ServiceScheduler;
