import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Colors from '../styles/Colors';
import { FontSizes } from '../styles/Typography';
import { format } from 'date-fns';
import { pl } from 'date-fns/locale'

const HistoryItem = props => {
  const { company, service, date } = props.visit;

  return (
    <View style={style.wrapper}>
      <View style={style.header}>
        <Text style={style.serviceName}>{service.name}</Text>
        <Text style={style.date}>
          {format(new Date(date), 'd LLLL y h:mm', {locale: pl})}
        </Text>
      </View>
      <Text style={style.companyName}>{company.name}</Text>
    </View>
  );
};

const style = StyleSheet.create({
  wrapper: {
    padding: 10,
    backgroundColor: Colors.purpleDark,
    borderTopWidth: 1,
    borderColor: Colors.greyLight,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  serviceName: {
    flex: 1,
    fontSize: FontSizes.base,
    color: Colors.purple,
  },
  companyName: {
    marginTop: 5,
    fontSize: FontSizes.small,
    color: Colors.white,
    opacity: 0.7,
  },
  date: {
    color: Colors.white,
  },
});

export default HistoryItem;
