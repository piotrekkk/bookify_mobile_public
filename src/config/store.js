import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import companiesReducer from '../reducers/Companies';
import businessTypesReducer from '../reducers/BusinessTypes';
import servicesReducer from '../reducers/Services';
import visitsReducer from '../reducers/Visits';

export default function configureStore() {
  const store = createStore(
    combineReducers({
      companies: companiesReducer,
      businessTypes: businessTypesReducer,
      services: servicesReducer,
      visits: visitsReducer,
    }),
    applyMiddleware(thunk),
  );

  return store;
}
