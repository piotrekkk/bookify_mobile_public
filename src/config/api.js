import axios from 'axios';
import { Platform } from 'react-native';
import Config from 'react-native-config';

const apiHost = __DEV__
  ? Platform.OS === 'android'
    ? Config.API_HOST_ANDROID
    : Config.API_HOST
  : Config.API_HOST;

const api = axios.create({
  baseURL: apiHost,
  headers: {
    'Content-Type': 'application/json',
  },
});

export default api;
