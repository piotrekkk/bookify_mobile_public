import api from '../config/api';
import { INDEX_VISITS_DATA } from '../reducers/Visits';

export const indexVisitsDataAction = ({ uniqDeviceId }) => {
  return async dispatch => {
    api.get('/visits', { params: { uniq_device_id: uniqDeviceId }}).then(({ data }) => {
      dispatch({ type: INDEX_VISITS_DATA, payload: data});
    });
  };
};

export const createVisitAction = ({ date, companyId, serviceId, uniqDeviceId, user }) => {
  return async () => {
    return new Promise((resolve, reject) => {
      api.post('/visits', {
        company_id: companyId,
        service_id: serviceId,
        uniq_device_id: uniqDeviceId,
        date,
        user,
      })
      .then(() => resolve())
      .catch(() => reject());
    });
  };
};
