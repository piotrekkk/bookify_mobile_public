import api from '../config/api';
import { INDEX_SERVICES_DATA } from '../reducers/Services';

export const indexServicesDataAction = ({ companyId }) => {
  return async dispatch => {
    api.get('/services', { params: { company_id: companyId }}).then(({ data }) => {
      dispatch({ type: INDEX_SERVICES_DATA, payload: { companyId, data }});
    });
  };
};
