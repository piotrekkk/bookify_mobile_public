import api from '../config/api';
import { INDEX_BUSINESS_TYPES_DATA } from '../reducers/BusinessTypes';

export const indexBusinessTypesDataAction = () => {
  return async dispatch => {
    api.get('/business_types').then(({ data }) => {
      dispatch({ type: INDEX_BUSINESS_TYPES_DATA, payload: { data }});
    });
  };
};
