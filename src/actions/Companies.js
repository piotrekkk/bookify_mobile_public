import api from '../config/api';
import { INDEX_COMPANIES_DATA } from '../reducers/Companies';

export const indexCompaniesDataAction = ({ businessTypeId }) => {
  return async dispatch => {
    api.get('/companies', { params: { business_type_id: businessTypeId }}).then(({ data }) => {
      dispatch({ type: INDEX_COMPANIES_DATA, payload: { businessTypeId, data }});
    });
  };
};
