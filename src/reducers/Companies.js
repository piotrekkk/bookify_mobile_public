export const companiesState = {
  all: {
    collection: [],
    page: null,
    page_size: null,
    total_size: null,
  },
};

export const INDEX_COMPANIES_DATA = 'INDEX_COMPANIES_DATA';

export default (state = companiesState, action) => {
  const { type, payload } = action;
  switch (type) {
    case INDEX_COMPANIES_DATA: {
      const { businessTypeId, data } = payload;
      return {
        ...state,
        [businessTypeId]: {
          collection: [...data.collection],
          page: data.page,
          page_size: data.page_size,
          total_size: data.total_size,
        },
      };
    }
    default:
      return state;
  }
};
