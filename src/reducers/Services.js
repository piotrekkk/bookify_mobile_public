export const servicesState = {};

export const INDEX_SERVICES_DATA = 'INDEX_SERVICES_DATA';

export default (state = servicesState, action) => {
  const { type, payload } = action;
  switch (type) {
    case INDEX_SERVICES_DATA: {
      const {companyId, data} = payload;
      return {
        ...state,
        [companyId]: {
          collection: [...data.collection],
          page: data.page,
          page_size: data.page_size,
          total_size: data.total_size,
        },
      };
    }
    default:
      return state;
  }
};
