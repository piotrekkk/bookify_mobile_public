export const businessTypesState = {
  collection: [],
  page: null,
  page_size: null,
  total_size: null,
};

export const INDEX_BUSINESS_TYPES_DATA = 'INDEX_BUSINESS_TYPES_DATA';

export default (state = businessTypesState, action) => {
  const { type, payload } = action;
  switch (type) {
    case INDEX_BUSINESS_TYPES_DATA: {
      const { data } = payload;
      return {
        collection: [...data.collection],
        page: data.page,
        page_size: data.page_size,
        total_size: data.total_size,
      };
    }
    default:
      return state;
  }
};
