export const visitsState = {};

export const INDEX_VISITS_DATA = 'INDEX_VISITS_DATA'

export default (state = visitsState, action) => {
  const { type, payload } = action;
  switch (type) {
    case INDEX_VISITS_DATA: {
      return {
        ...state,
        collection: [...payload.collection],
        page: payload.page,
        page_size: payload.page_size,
        total_size: payload.total_size,
      };
    }
    default:
      return state;
  }
};
