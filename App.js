import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import RootScreen from './src/screens/RootScreen';
import BusinessTypePickerScreen from './src/screens/BusinessTypePickerScreen';
import CompanyScreen from './src/screens/CompanyScreen';
import ServiceScreen from './src/screens/ServiceScreen';
import BookVisitScreen from './src/screens/BookVisitScreen';
import HistoryScreen from './src/screens/HistoryScreen';
import configureStore from './src/config/store';
import Colors from './src/styles/Colors';

const store = configureStore();
const Stack = createStackNavigator();

const defaultScreenOptions = {
  headerBackTitle: ' ',
  headerTintColor: Colors.white,
  headerStyle: {
    backgroundColor: Colors.greyLight,
    shadowOffset: {
      height: 0,
    },
    elevation: 0,
  },
};

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen name="Root" component={RootScreen} options={{headerShown: false}} />
            <Stack.Screen name="Company" component={CompanyScreen} options={defaultScreenOptions} />
            <Stack.Screen name="Service" component={ServiceScreen} options={defaultScreenOptions} />
            <Stack.Screen name="BookVisit" component={BookVisitScreen} options={defaultScreenOptions} />
            <Stack.Screen name="BusinessTypePicker" component={BusinessTypePickerScreen} options={{...defaultScreenOptions, headerTitle: ''}} />
            <Stack.Screen name="History" component={HistoryScreen} options={defaultScreenOptions} />
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    );
  }
}

export default App;
